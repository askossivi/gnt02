Dockerfile:
#Use a node base image
FROM node:7-onbuild

# Set maitainer - optinal
LABEL maintainer "kossivi@devops.com

# Set a health check - FOR DOCKER TO TELL IF THE SERVER IS UP OR NOT
HEALTHCHECK --interval=5s \
            --timeout=5s \
			CMD curl -f http://127.0.0.1:8000 || exit 1

# Tell docker what port to expose
EXPOSE 8000
